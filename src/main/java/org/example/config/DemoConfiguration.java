package org.example.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
public class DemoConfiguration {
    @Value("${demo.test.hello}")
    private String testHello;
}
