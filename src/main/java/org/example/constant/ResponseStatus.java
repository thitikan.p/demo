package org.example.constant;

import lombok.experimental.UtilityClass;

@UtilityClass
public class ResponseStatus {
    public static final String SUCCESS = "success";
    public static final String FAILED = "failed";
}
