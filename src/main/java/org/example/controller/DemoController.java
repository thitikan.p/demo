package org.example.controller;

import lombok.RequiredArgsConstructor;
import org.example.config.DemoConfiguration;
import org.example.controller.request.DemoRequest;
import org.example.controller.response.DemoListResponse;
import org.example.controller.response.DemoResponse;
import org.example.controller.response.GeneralResponse;
import org.example.controller.response.Pagination;
import org.example.service.DemoService;
import org.example.service.dto.DemoDto;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.example.constant.ResponseStatus.SUCCESS;

@RestController
@RequiredArgsConstructor
public class DemoController {

    private final DemoConfiguration demoConfiguration;
    private final DemoService demoService;
    private final ModelMapper modelMapper;

    @GetMapping("/hello")
    public String hello() {
        return "Hello " + demoConfiguration.getTestHello();
    }

    @PostMapping("/create")
    public GeneralResponse<DemoResponse> createDemo(@RequestBody DemoRequest request) {
        DemoDto demoReq = demoService.createDemo(request);
        DemoResponse demoResponse = modelMapper.map(demoReq, DemoResponse.class);
        return  new GeneralResponse<>(SUCCESS, demoResponse);
    }

    @GetMapping("/list")
    public GeneralResponse<DemoListResponse> getListDemo(
            @RequestParam(name="page", defaultValue = "1") int pageIndex,
            @RequestParam(name="size", defaultValue = "10") int pageSize
    ) {
        final Page<DemoDto> demoDtoPage = demoService.getList(pageIndex, pageSize);
        Pagination pagination = new Pagination();
        pagination.setPage(demoDtoPage.getPageable().getPageNumber());
        pagination.setSize(demoDtoPage.getSize());
        pagination.setTotalPage(demoDtoPage.getTotalPages());
        pagination.setTotalRecords(demoDtoPage.getTotalElements());

        List<DemoResponse> demoList = new ArrayList<>();
        demoDtoPage.forEach(demoDto -> demoList.add(modelMapper.map(demoDto, DemoResponse.class)));

        final DemoListResponse demoResponse = new DemoListResponse();
        demoResponse.setPagination(pagination);
        demoResponse.setDemoResponseList(demoList);

        return new GeneralResponse<>(SUCCESS, demoResponse);
    }

    @PutMapping("/update/{id}")
    public GeneralResponse<DemoResponse> updateDemo(
            @PathVariable("id") UUID demoId,
            @RequestBody DemoRequest request
    ) {
        DemoDto demoReq = demoService.updateDemo(demoId, request);
        DemoResponse demoResponse = modelMapper.map(demoReq, DemoResponse.class);
        return  new GeneralResponse<>(SUCCESS, demoResponse);
    }

    @DeleteMapping("/delete/{id}")
    public GeneralResponse<String> deleteDemo(@PathVariable("id") UUID demoId) {
        demoService.deleteDemo(demoId);
        return  new GeneralResponse<>(SUCCESS, "ID: " + demoId + " has been deleted!");
    }
}
