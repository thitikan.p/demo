package org.example.controller.request;

import lombok.Data;

@Data
public class DemoRequest {
    private String demoCarBrand;
    private Integer demoCarQuantity;
}
