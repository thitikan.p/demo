package org.example.controller.response;

import lombok.Data;

import java.util.List;

@Data
public class DemoListResponse {
    private Pagination pagination;
    private List<DemoResponse> demoResponseList;
}
