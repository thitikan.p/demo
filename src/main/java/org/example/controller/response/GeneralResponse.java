package org.example.controller.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

public class GeneralResponse<T> {
    @JsonProperty("status")
    private String status;
    @JsonProperty("data")
    private T data;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("message")
    private String message;

    public GeneralResponse(String status, T data) {
        this.status = status;
        this.data = data;
    }

    public GeneralResponse(String status) {
        this.status = status;
    }
}
