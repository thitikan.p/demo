package org.example.controller.response;

import lombok.Data;

@Data
public class Pagination {
    private Integer page;
    private Integer size;
    private Integer totalPage;
    private Long totalRecords;

    public void setPage(Integer page) {
        this.page = page + 1;
    }
}
