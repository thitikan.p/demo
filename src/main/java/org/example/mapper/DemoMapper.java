package org.example.mapper;

import org.example.entity.DemoEntity;
import org.example.service.dto.DemoDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface DemoMapper {
    DemoDto toDemoDto(DemoEntity demoEntity);
}
