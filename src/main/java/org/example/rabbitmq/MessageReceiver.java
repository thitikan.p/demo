package org.example.rabbitmq;

import lombok.RequiredArgsConstructor;
import org.example.entity.EmailEntity;
import org.example.service.EmailService;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class MessageReceiver {

    private final EmailService emailService;
    @RabbitListener(queues = "myQueue")
    public void listen(String in) {
        System.out.println("Message read from myQueue : " + in);
        EmailEntity details = new EmailEntity();
        details.setRecipient("thitikan.p@evme.io");
        details.setSubject("Demo Test Message RabbitMQ");
        details.setMsgBody("Message from Sender is : " + in);
        String status = emailService.sendSimpleEmail(details);
        System.out.println("Send Mail! : " + status);
    }
}
