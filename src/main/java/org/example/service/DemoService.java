package org.example.service;

import lombok.RequiredArgsConstructor;
import org.example.controller.request.DemoRequest;
import org.example.entity.DemoEntity;
import org.example.mapper.DemoMapper;
import org.example.rabbitmq.MessageSender;
import org.example.repository.jpa.DemoRepository;
import org.example.service.dto.DemoDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class DemoService {

    private final MessageSender sender;
    private final DemoRepository demoRepository;
    private final DemoMapper demoMapper;
    public DemoDto createDemo(DemoRequest request) {
        DemoEntity demoEntity = new DemoEntity();
        var timeNow = new Date();
        Timestamp timestamp = new Timestamp(timeNow.getTime());
        demoEntity.setDemoCarBrand(request.getDemoCarBrand());
        demoEntity.setDemoCarQuantity(request.getDemoCarQuantity());
        demoEntity.setCreatedAt(timestamp);
        demoEntity.setUpdatedAt(timestamp);
        DemoEntity resultEntity = demoRepository.save(demoEntity);
        sender.send();

        DemoDto response = demoMapper.toDemoDto(resultEntity);

        return response;
    }

    public Page<DemoDto> getList(int pageIndex, int pageSize) {
        Pageable pageable = PageRequest.of(pageIndex - 1, pageSize);
        Page<DemoEntity> demo = demoRepository.findAll(pageable);
        Page<DemoDto> demoDtoPage = demo.map(demoMapper::toDemoDto);

        return demoDtoPage;
    }

    public DemoDto updateDemo(UUID id, DemoRequest request) {
        DemoEntity demoEntity = demoRepository.getById(id);
        var timeNow = new Date();
        Timestamp timestamp = new Timestamp(timeNow.getTime());

        demoEntity.setDemoCarBrand(request.getDemoCarBrand());
        demoEntity.setDemoCarQuantity(request.getDemoCarQuantity());
        demoEntity.setUpdatedAt(timestamp);

        DemoEntity resultEntity = demoRepository.save(demoEntity);
        DemoDto response = demoMapper.toDemoDto(resultEntity);

        return response;
    }

    public void deleteDemo(UUID id) {
        demoRepository.deleteById(id);
    }
}
