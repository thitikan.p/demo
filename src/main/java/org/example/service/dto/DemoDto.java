package org.example.service.dto;

import lombok.Data;

import java.sql.Timestamp;
import java.util.UUID;

@Data
public class DemoDto {
    private UUID id;
    private String demoCarBrand;
    private Integer demoCarQuantity;
    private Timestamp createdAt;
    private Timestamp updatedAt;
}
